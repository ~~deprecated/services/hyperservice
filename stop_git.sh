#!/bin/sh

# Hyperservices, scripts to run chroot services.
# Copyright (C) 2017 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CHROOT_DIR=/srv/data/git

[ -f $CHROOT_DIR/run/uwsgi_cgit.pid ] && sudo unshare -im chroot $CHROOT_DIR /usr/bin/uwsgi   --stop /etc/uwsgi/cgit.ini
[ -f $CHROOT_DIR/run/nginx.pid      ] && sudo unshare -im chroot $CHROOT_DIR /usr/bin/kill --verbose $(cat $CHROOT_DIR/run/nginx.pid)
[ -f $CHROOT_DIR/run/sshd.pid       ] && sudo unshare -im chroot $CHROOT_DIR /usr/bin/kill --verbose $(cat $CHROOT_DIR/run/sshd.pid )

sleep 1s

mountpoint -q $CHROOT_DIR/sys     ; MP=$? && [ $MP = 0 ] && sudo umount -v $CHROOT_DIR/sys
mountpoint -q $CHROOT_DIR/run     ; MP=$? && [ $MP = 0 ] && sudo umount -v $CHROOT_DIR/run
mountpoint -q $CHROOT_DIR/dev/pts ; MP=$? && [ $MP = 0 ] && sudo umount -v $CHROOT_DIR/dev/pts
mountpoint -q $CHROOT_DIR/dev     ; MP=$? && [ $MP = 0 ] && sudo umount -v $CHROOT_DIR/dev
mountpoint -q $CHROOT_DIR/proc    ; MP=$? && [ $MP = 0 ] && sudo umount -v $CHROOT_DIR/proc
unset CHROOT_DIR MP
