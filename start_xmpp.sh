#!/bin/sh

# Hyperservices, scripts to run chroot services.
# Copyright (C) 2017 Márcio Silva <coadde@hyperbola.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CHROOT_DIR=/srv/data/xmpp

mountpoint -q $CHROOT_DIR/proc    ; MP=$? && [ $MP = 1 ] && sudo mount -vt proc     proc     -o relatime,nodev,nosuid,noexec $CHROOT_DIR/proc
mountpoint -q $CHROOT_DIR/dev     ; MP=$? && [ $MP = 1 ] && sudo mount -vt devtmpfs devtmpfs -o relatime,dev,suid,exec       $CHROOT_DIR/dev
mountpoint -q $CHROOT_DIR/dev/pts ; MP=$? && [ $MP = 1 ] && sudo mount -vt devpts   devpts   -o relatime,dev,nosuid,noexec   $CHROOT_DIR/dev/pts
mountpoint -q $CHROOT_DIR/run     ; MP=$? && [ $MP = 1 ] && sudo mount -vt tmpfs    tmpfs    -o relatime,nodev,nosuid,noexec $CHROOT_DIR/run
mountpoint -q $CHROOT_DIR/sys     ; MP=$? && [ $MP = 1 ] && sudo mount -vt sysfs    sysfs    -o relatime,nodev,nosuid,noexec $CHROOT_DIR/sys
unset MP

sudo unshare -im chroot $CHROOT_DIR /usr/bin/ssh-keygen -vA &
[ ! -f $CHROOT_DIR/run/sshd.pid            ] && sudo unshare -im chroot $CHROOT_DIR /usr/bin/sshd &
[ ! -f $CHROOT_DIR/run/prosody/prosody.pid ] && sudo unshare -im chroot $CHROOT_DIR /usr/bin/prosodyctl start &
unset CHROOT_DIR
